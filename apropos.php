<?php
/*Minute Papillon -- 06/09/2020
 *
 * apropos.php
 * Auteur : Acryline Erin
 * Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 3.0 France.
 * 
 */

include("_header.inc.php");
?>
<div class="w3-padding w3-margin w3-white aide ">

		<button onclick="accordion('auteur')" class="w3-button w3-block w3-left-align w3-large w3-padding-24 w3-lime w3-card-2  w3-round"><b> 🦋 Auteur </b></button>
		<div id="auteur" class="w3-container w3-hide">
		  <p>
		  		<div>
		  		<strong>Acryline Erin </strong>
				 <a rel="me" href="https://framapiaf.org/@acryline">
				 <img src="https://framapiaf.org/packs/logo-fe5141d38a25f50068b4c69b77ca1ec8.svg" 
				 											alt="Mastodon" width="32px" height="32px"/></a>
				 <a href="https://framasphere.org/people/337ae2207a5b01334c722a0000053625">
				 <img src="images/diaspora.png" alt="Diaspora" width="32px" height="32px"/></a> 
				</div>
		  </p>
		</div><br>

		<button onclick="accordion('version')" class="w3-button w3-block w3-left-align w3-large w3-padding-24 w3-lime w3-card-2  w3-round"><b> 🦋 Version </b></button>
		<div id="version" class="w3-container w3-hide">
		  <p><strong>Minute Papillon V 0.1</strong> 
		     <br> Le code du site est libre et peut être consulté et téléchargé <a href="https://framagit.org/acryline/le-procrastinateur/" >à cette adresse.</a> </li> 
		  </p>

		</div><br>	

		<button onclick="accordion('CGU')" class="w3-button w3-block w3-left-align w3-large w3-padding-24 w3-lime w3-card-2  w3-round"><b> 🦋 Conditions Générales d'Utilisation </b></button>
		<div id="CGU" class="w3-container w3-hide">
				<br>Le présent document a pour objet de définir les modalités et conditions dans lesquelles d’une part, 
				Acryline Erin, ci-après dénommée l’EDITEUR, met à la disposition de ses utilisateurs le site 
				<b>Minute Papillon</b>, et les services associés et d’autre part, la manière par laquelle l’utilisateur
				accède au site et utilise ses services.
				<br>Toute connexion au site  est subordonnée au respect des présentes conditions.

				<br><h3> Propriété intellectuelle </h3>
				Tous les éléments de ce site, sont libres de droit. La reproduction des pages de ce site est autorisée 
				à la condition d’y mentionner la source. Elles ne peuvent être utilisées à des fins commerciales et 
				publicitaires. <a href="http://creativecommons.org/licenses/by-nc/3.0/fr/" >Licence
				Creative Commons Attribution - Pas d’Utilisation Commerciale 3.0 France</a>. 		
				
				<br><h3>Liens hypertextes et téléporteurs</h3>
				Le site <b>Minute Papillon </b> peut contenir des liens hypertextes vers d’autres sites présents sur le réseau Internet. 
				Les liens vers ces autres ressources vous font quitter le site <b>Minute Papillon </b> .
					
				<h3>Responsabilité de l’éditeur</h3>
				L’EDITEUR ne pourra en aucun cas être tenu responsable de tout dommage de quelque nature qu’il soit 
				résultant de l’interprétation ou de l’utilisation des informations et/ou documents disponibles sur ce site.
				
				<br><h3>Responsabilité de l'utilisateur</h3>
				L'utilisateur est responsable de la sécurité de ses listes de tâches et de son navigateur.
				<br>L'utilisateur ne peut pas utiliser le service du site et de la grille <b>Minute Papillon </b>  à des fins 
				illégales ou non autorisées.
				<br>L’utilisateur est seul responsable de l’usage des données qu’il consulte, interroge et transfère sur Internet.
								
   			<h3>Accès au site Minute Papillon  </h3>
				L’éditeur s’efforce de permettre l’accès au site 24 heures sur 24, 7 jours sur 7, sauf en cas de force 
				majeure ou d’un événement hors du contrôle de l’EDITEUR, et sous réserve des éventuelles pannes et 
				interventions de maintenance nécessaires au bon fonctionnement du site et des services.
				<br>
				Par conséquent, l’EDITEUR ne peut garantir une disponibilité du site et/ou des services, une fiabilité 
				des transmissions et des performances en terme de temps de réponse ou de qualité. Il n’est prévu aucune
				assistance technique vis à vis de l’utilisateur que ce soit par des moyens électronique ou téléphonique.
				<br>
				La responsabilité de l’éditeur ne saurait être engagée en cas d’impossibilité d’accès à ce site et/ou 
				d’utilisation des services.
				<br>
				Par ailleurs, l’EDITEUR peut être amené à interrompre le site ou une partie des services, à tout moment 
				sans préavis, le tout sans droit à indemnités. L’utilisateur reconnaît et accepte que l’EDITEUR ne soit 
				pas responsable des interruptions, et des conséquences qui peuvent en découler pour l’utilisateur ou tout tiers.
				
				<br><h3>Modification des conditions d’utilisation</h3>
				L’EDITEUR se réserve la possibilité de modifier, à tout moment et sans préavis, les présentes 
				conditions d’utilisation afin de les adapter aux évolutions du site et/ou de son exploitation.
				
				<br><h3>Droit applicable</h3>
				Tant le présent site que les modalités et conditions de son utilisation sont régis par le droit français, 
				quel que soit le lieu d’utilisation. En cas de contestation éventuelle, et après l’échec de toute tentative 
				de recherche d’une solution amiable, les tribunaux français seront seuls compétents pour connaître de ce litige.
				
				Pour toute question relative aux présentes conditions d’utilisation du site, vous pouvez  envoyer un message
				privé à acryline erin sur Framasphère ou Mastodon. 
				
			
				<br><h3>Données personnelles</h3>
 				Aucune donnée personnelle n'est enregistrée.
 				
				<br><br><small><div id="note1"> CGU en partie inspirées de l'article
				<a href="http://www.droitissimo.com/contrat/conditions-generales-dutilisation-cgu-dun-site-internet" />
				"Modèle de CGU d'un site internet"</a></div> <br>
				 </small>
		</div><br>

		<button onclick="accordion('changelog')" class="w3-button w3-block w3-left-align w3-large w3-padding-24 w3-lime w3-card-2  w3-round"><b> 🦋 Log des changements </b></button>
		<div id="changelog" class="w3-container w3-hide">
		<div class='w3-medium' style='padding-left:20px;'>
				 <textarea class="w3-input" disabled= 'disabled' style="margin: 2px; width: 100%; height: 800px; color: black; background-color:white;">
			<?php include "CHANGELOG";?>
			</textarea> 
        </div>
	    </div><br>


		<button onclick="accordion('licence')" class="w3-button w3-block w3-left-align w3-large w3-padding-24 w3-lime w3-card-2  w3-round"><b> 🦋 Licence </b></button>
		<div id="licence" class="w3-container w3-hide">
		<div class='w3-medium' style='padding-left:20px;'>
			<br><a rel='license' href='http://creativecommons.org/licenses/by-nc/3.0/fr/'>
			<img alt='Licence Creative Commons' style='border-width:0' src='https://i.creativecommons.org/l/by-nc/3.0/fr/88x31.png' /></a>
			Acryline-Erin -- Septembre 2020-- Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 3.0 France</a>.
		</div>
		</div><br>										
</div>
<?php
include("_footer.inc.php");
?>