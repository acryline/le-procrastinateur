<?php
/*Le Procrastinateur -- 29/08/2020
 *
 * _header.inc.php
 * Auteur : Acryline Erin
 * Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 3.0 France.
 * 
 */
   session_start();
   include("fonctions.php");
   $tabTaches = array();
   $tpsCourt = 480;
   $tpsLong = 2400;
	if(!isset($_SESSION['Liste'])) $_SESSION['Liste']  = "";
	if(!isset($_SESSION['tpsCourt'])) $_SESSION['tpsCourt']= 0;
	if(!isset($_SESSION['tpsLong'])) $_SESSION['tpsLong']  = 0; 
   
   if(!isset($_SESSION['flag']))
   {
   	$_SESSION['flag']=0;
   }
?>
<!DOCTYPE html>
<html >
	<head>
		<title>🦋 Minute Papillon : </title>
		<meta charset='utf-8'/>
		<link rel="stylesheet" href="css/w3.css">
		<link rel="stylesheet" href="css/style.css">
	</head>
	 <body class="w3-lime" id="recharger" onload="chargerStorage(<?php echo $_SESSION['flag'];?>);">
		<header >
			<div class="w3-padding w3-xxlarge lobster" >Minute Papillon ! 🦋 </div>
				<nav>
						<div class="w3-bar w3-green w3-xlarge">
						  <a href="index.php" class="w3-bar-item  w3-btn w3-hover-light-green">Listes</a>
						  <a href="gestion.php" class="w3-bar-item w3-btn w3-hover-light-green">Gestion</a>
						  <a href="aide.php" class="w3-bar-item w3-btn w3-hover-light-green">Aide</a>
						  <a href="apropos.php" class="w3-bar-item w3-btn w3-hover-light-green">À propos</a>
						</div>
						<?php
						   $_SESSION['emoji'] = chargerEmojis();
							include("_modal.inc.php"); 
						?>
				</nav><br>
		</header>
		<section  >
     
		<?php
		   echo "<div id='variables'></div>";	
   		$tabTaches = json_decode($_SESSION['Liste'],TRUE);
			$tpsCourt = $_SESSION['tpsCourt'];
		  	$tpsLong = 	$_SESSION['tpsLong'];
  		?>		