<?php

if(isset($_GET['typeChrono']))
{
	$typeChrono = trim($_GET['typeChrono']);
	if($typeChrono == 'court')
	 {
	 	?>
		<label class="w3-padding w3-white w3-margin-left" id="chrono_court">00 h : 00 mn : 00 s </label>
		&nbsp;&nbsp;&nbsp;<button class='w3-btn w3-green' id='btn-pause-court' onclick="pauseChrono('<?php echo $typeChrono; ?>');"> Pause</button>
	 	&nbsp;&nbsp;&nbsp;<button class='w3-btn w3-green' id='btn-stop-court' onclick="arreterChrono('<?php echo $typeChrono; ?>');"> Arrêter</button><br><br>
	 	<?php
	 }
	elseif($typeChrono == 'long') 
	 {
	 	?>
		<label class="w3-padding w3-white w3-margin-left" id="chrono_long">00 h : 00 mn : 00 s</label>
		&nbsp;&nbsp;&nbsp;<button class='w3-btn w3-green' id='btn-pause-long' onclick="pauseChrono('<?php echo $typeChrono; ?>');"> Pause</button>
	 	&nbsp;&nbsp;&nbsp;<button class='w3-btn w3-green' id='btn-stop-long' onclick="arreterChrono('<?php echo $typeChrono; ?>');"> Arrêter</button><br><br>
	 	<?php
	 }
}