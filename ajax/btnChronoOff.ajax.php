<?php

if(isset($_GET['typeChrono']))
{
	$typeChrono = trim($_GET['typeChrono']);
	if($typeChrono == 'court')
	 {
	 	?>
		<label class="w3-padding w3-white w3-margin-left" id="chrono_court">00 h : 00 mn : 00 s </label>
	 	&nbsp;&nbsp;&nbsp;<button class="w3-btn w3-green" id="btn-go-court" onclick="lancerChrono('court');">Démarrer</button><br><br>
	 	<?php
	 }
	elseif($typeChrono == 'long') 
	 {
	 	?>
		<label class="w3-padding w3-white w3-margin-left" id="chrono_long">00 h : 00 mn : 00 s</label> 
	 	&nbsp;&nbsp;&nbsp;<button class="w3-btn w3-green" id="btn-go-long" onclick="lancerChrono('long');"> Démarrer</button><br><br>
	 	<?php
	 }
}