<?php

if(isset($_GET['typeChrono']) && isset($_GET['etat']) )
{
   $etat = trim($_GET['etat']);  
 	$typeChrono = trim($_GET['typeChrono']);
 	
	if($typeChrono == 'court')
	 {
	 	?>
	 	<label class="w3-padding w3-white w3-margin-left" id="chrono_court"> <?php echo $etat; ?></label>
	 	&nbsp;&nbsp;&nbsp;<button class='w3-btn w3-green' id='btn-go-court' onclick="lancerChrono('<?php echo $typeChrono; ?>');"> Redémarrer</button>
	 	&nbsp;&nbsp;&nbsp;<button class='w3-btn w3-green' id='btn-stop-court' onclick="arreterChrono('<?php echo $typeChrono; ?>');"> Arrêter</button><br><br>
	 	<?php
	 }
	elseif($typeChrono == 'long') 
	 {
	 	?>
	 	<label class="w3-padding w3-white w3-margin-left" id="chrono_long"><?php echo $etat; ?></label>
	 	&nbsp;&nbsp;&nbsp;<button class='w3-btn w3-green' id='btn-go-long' onclick="lancerChrono('<?php echo $typeChrono; ?>');"> Redémarrer</button>
	 	&nbsp;&nbsp;&nbsp;<button class='w3-btn w3-green' id='btn-stop-long' onclick="arreterChrono('<?php echo $typeChrono; ?>');"> Arrêter</button><br><br>
	 	<?php
	 }
}