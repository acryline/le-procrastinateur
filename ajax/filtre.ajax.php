<?php
session_start();
include ("../fonctions.php");

$filtre_type = "tout";
$filtre_statut = "tout";

if (isset($_GET['filtre_type']) && isset($_GET['filtre_statut']))
{
  $filtre_type = $_GET['filtre_type'];
  $filtre_statut =  $_GET['filtre_statut'];
}

$tabTaches = json_decode($_SESSION['Liste'],TRUE);
$resultat = array();

if($filtre_type!= 'tout')
{
	foreach ($tabTaches as $tache)
	{
	   if($filtre_type == $tache['type_liste'] )
	   {
	   	array_push($resultat,$tache);
	   }
	}
	$tabTaches = $resultat;
	$resultat = array();
}
if($filtre_statut!= 'tout')
{
	foreach ($tabTaches as $tache)
	{
	   if($filtre_statut == $tache['statut'] )
	   {
	   	array_push($resultat,$tache);
	   }
	}
	$tabTaches = $resultat;
}

afficherTableau($tabTaches, $filtre_type, $filtre_statut);