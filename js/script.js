/*Le Procrastinateur -- 06/09/2020
*
* js/script.js
* Auteur : Acryline Erin
* Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 3.0 France.
* 
*/ 
init();

function fermerModal()
{
	var checkbox = document.getElementById('fermer_def').checked;
	if(checkbox) 
	{
		localStorage.setItem('primo',1);
	}
	document.getElementById('modal1').style.display='none';
}

function fermerAlert()
{
	document.getElementById('modal2').style.display='none';
}

function init()
{
  //Premier accès ?
  var primo = localStorage.getItem('primo');
  if(!primo)
  {
  	 document.getElementById('modal1').style.display = "";
  }
  //Taille max d'une variable 	
  var max = localStorage.getItem('size');
  if(!max )
  {
    tailleMax();
  }
  
  //Charger ou définir les timers
  var timerCourt = localStorage.getItem('timerCourt');
  if (!timerCourt) 
  {
  	localStorage.setItem('timerCourt',480.0);
  }
  localStorage.setItem('timerCourtActif',timerCourt); 
  
  var timerLong = localStorage.getItem('timerLong');
  if (!timerLong) 
  {
  	localStorage.setItem('timerLong',2400.0);
  }
  localStorage.setItem('timerLongActif',timerLong);   
  
  //Charger les listes
  var liste = localStorage.getItem('liste');
  if (!liste) 
  {
  	 localStorage.setItem('liste','{}');
  }
}

function tailleMax()
{
	if (localStorage && !localStorage.getItem('size')) {
	    var i = 0;
	    try {
	        // Test 
	        for (i = 250; i <= 10000; i += 250) {
	            localStorage.setItem('test', new Array((i * 1024) + 1).join('a'));
	        }
	    } catch (e) {
	        localStorage.removeItem('test');
	        localStorage.setItem('size', i - 250);            
	    }
	}
	return localStorage.getItem('size');
}

function chargerStorage(flag) 
{
	var Liste = localStorage.getItem('liste');
	var tpsCourt = localStorage.getItem('timerCourt');
	var tpsLong = localStorage.getItem('timerLong');
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById('variables').value = this.responseText;
			if (flag == 0)
         {			
				location.reload();
			}
		}
	};

	xhttp.open("GET", "storage.ajax.php?Liste="+Liste +"&tpsCourt="+tpsCourt+"&tpsLong="+tpsLong, true);
	xhttp.send();
}

function chargerListe()
{
	var Liste = localStorage.getItem('liste');
 	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById('recharger').value = this.responseText;
				setTimeout(function (){location.reload()}, 1000);
		}
	};
	xhttp.open("GET", "ajax/rechargerListe.ajax.php?Liste="+Liste, true);
	xhttp.send();  
}

function afficherSelection() 
{
  var selection = document.getElementsByClassName('radio');
  var nbrTaches = selection.length;
  var id = null;
  var emoji = '';
  var tache = '';
  var typeTache = '';
  var etat = '';
  for(var i = 0; i< nbrTaches; i++)
  {
  	  if(selection[i].checked)
  	  {
  	  	   var parentElt = selection[i].parentElement.parentElement;
  	  	   id = i;
  	  	   emoji = parentElt.children[0].innerText;
  	  	   tache = parentElt.children[1].innerText;
  	  	   typeTache = parentElt.children[2].innerText
  	  	   etat = parentElt.children[3].innerText
  	  }
  }
  var xhttp = new XMLHttpRequest();
  	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById('modifTache').innerHTML = this.responseText;
		}
	};

	xhttp.open("GET", "selectTache.ajax.php?id="+id+"&emoji="+emoji +"&tache="+tache+"&typeTache="+typeTache+"&etat="+etat, true);
	xhttp.send();
}

function idPremiereTache(type_liste)
{
   var tabListe = JSON.parse(localStorage.getItem('liste'));
   console.log(tabListe);
   var nbr = tabListe.length; 
   for(var i =0; i<nbr;i++)
   {
   	if(tabListe[0]==null) return null;
      if(tabListe[i]['type_liste'] == type_liste && tabListe[i]['statut'] == 'actif' )
      {
        id = i;
        return id;        
      }
   }
   return null; 
}

function idDerniereTache(type_liste)
{
	var tabListe = JSON.parse(localStorage.getItem('liste'));
   var nbr = tabListe.length;
   var id = null; 
   for(var i =0; i<nbr;i++)
   {
   	if(tabListe[0]==null) return null;
      if(tabListe[i]['type_liste'] == type_liste && tabListe[i]['statut'] == 'actif' )
      {
        id = i;
      }   	
   }
   return id;    
}

function placerTacheFin(id)
{
   var tabRep = [];	
   var tabListe = JSON.parse(localStorage.getItem('liste'));
   var nbr = tabListe.length; 
   if(id != null )
   {
	   for(var i =0; i<nbr;i++)
	   {
	   	if(i!=id ) tabRep.push(tabListe[i]);
	   }
   tabRep.push(tabListe[id]);
   var json = JSON.stringify(tabRep);
   if(json != null) localStorage.setItem('liste',json);
   }  
}

function placerTacheDebut(id)
{
	var tabRep = [];
   var tabListe = JSON.parse(localStorage.getItem('liste'));
   var nbr = tabListe.length;	
   if(id != null )
   {
   	tabRep.push(tabListe[id]);   
	   for(var i =0; i<nbr;i++)
	   {
	   	if(i!=id ) tabRep.push(tabListe[i]);
	   }  
   var json = JSON.stringify(tabRep);
   if(json != null) localStorage.setItem('liste',json); 	
   }
}

function archiverId(id)
{
   var tabListe = JSON.parse(localStorage.getItem('liste')); 
   var nbr = tabListe.length;
   tabListe[id]['statut'] = 'archive';  
   var json = JSON.stringify(tabListe);	
   if(json != null) localStorage.setItem('liste',json);
}

function passerTache(type_liste)
{
  var id = idPremiereTache(type_liste);
  if(id !=null) placerTacheFin(id);
  chargerListe(); 
}


function archiverTache(type_liste)
{
  var id = idPremiereTache(type_liste);
  if(id !=null) archiverId(id);
  chargerListe(); 
}

function retourTache(type_liste)
{
 var id = idDerniereTache(type_liste);	
 if(id !=null) placerTacheDebut(id);
 chargerListe();
}

function purgerData()
{
  var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById('purger').innerHTML = this.responseText;
		}
	};
	xhttp.open("GET","_alert.modal.php" , true);
	xhttp.send();   
}

function accordion(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else {
    x.className = x.className.replace(" w3-show", "");
  }
}

function selectEmoji(cle, idEmoji)
{
  var emoji = document.getElementsByName(cle)[0].innerHTML;
  var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById(idEmoji.id).innerHTML = this.responseText;
		}
	};
	xhttp.open("GET","ajax/comboEmoji.ajax.php?emoji="+emoji , true);
	xhttp.send();   
}

function filtre() 
{
	var i=0;
	var select_type = document.getElementById('filtre').children[1].getElementsByTagName('input');
	for(i=0;i<3;i++)
	{
		if (select_type[i].checked) 
		{
			filtre_type = select_type[i].value;
		}
	}
	var select_statut = document.getElementById('filtre').children[2].getElementsByTagName('input');
 	for(i=0;i<3;i++)
	{
		if (select_statut[i].checked) 
		{
			filtre_statut = select_statut[i].value;
		}
	}

  var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById('tableau_taches').innerHTML = this.responseText;
		}
	};
	xhttp.open("GET","ajax/filtre.ajax.php?filtre_type="+filtre_type+"&filtre_statut="+filtre_statut , true);
	xhttp.send();   
}