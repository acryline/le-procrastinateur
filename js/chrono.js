/*Le Procrastinateur -- 01/09/2020
 *
 * fonctions.php
 * Auteur : Acryline Erin
 * Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 3.0 France.
 * 
 */

var audio = new Audio('son/sonnette.mp3');

function lancerChrono(typeC)
{
  if(typeof(timer) != 'undefined') clearInterval(timer);
  typeChrono = typeC;
  if(typeChrono == 'court')
  {
  	temps = localStorage.getItem('timerCourtActif');
	localStorage.setItem('timerLongActif',localStorage.getItem('timerLong'));
  }
  else if(typeChrono == 'long') {
  	temps = localStorage.getItem('timerLongActif');
	localStorage.setItem('timerCourtActif',localStorage.getItem('timerCourt'));  
  }
  boutonsChronoOn(typeChrono);
  timer =setInterval("CompteaRebour()",1000);
}

function arreterChrono(typeChrono)
{
   if(typeof(timer) != 'undefined') clearInterval(timer);
   if(typeChrono == 'court') 
    {
    	localStorage.setItem('timerCourtActif',localStorage.getItem('timerCourt'));
    }  
   if(typeChrono == 'long') 
    {
   		localStorage.setItem('timerLongActif',localStorage.getItem('timerLong'));
    }    
   boutonsChronoOff(typeChrono);
}

function pauseChrono(typeChrono)
{
  if(typeof(timer) != 'undefined') clearInterval(timer);
  boutonsChronoPause(typeChrono);
}

//Tiré de https://phpsources.net/code_s.php?id=493
//Auteur           : KOogar
function CompteaRebour()
{
  temps-- ;
  j = parseInt(temps) ;
  h = parseInt(temps/3600) ;
  m = parseInt((temps%3600)/60) ;
  s = parseInt((temps%3600)%60) ;
  var type_liste = '';

 if(typeChrono == 'court')
 {
 type_liste = 'courte';  
 localStorage.setItem('timerCourtActif',temps);	
 document.getElementById('chrono_court').innerHTML= (h<10 ? "0"+h : h) + '  h :  ' +
                                                (m<10 ? "0"+m : m) + ' mn : ' +
                                                (s<10 ? "0"+s : s) + ' s ';
 }
 else if(typeChrono == 'long')
 {
 type_liste='longue';
 localStorage.setItem('timerLongActif',temps);
 document.getElementById('chrono_long').innerHTML= (h<10 ? "0"+h : h) + '  h :  ' +
                                                (m<10 ? "0"+m : m) + ' mn : ' +
                                                (s<10 ? "0"+s : s) + ' s ';
 }
 
	if ((s == 0 && m ==0 && h ==0)) {
	   finaliserChrono(type_liste);
	}
}

function finaliserChrono(type_liste)
{
  var duree = audio.duration;
  var id;
  //Trouver la première tâche
   id = idPremiereTache( type_liste);
  //Construire la nouvelle liste
   placerTacheFin(id);
  //Actualisation de la page
   clearInterval(timer);
   audio.play();
   setTimeout(chargerListe(), duree*1000);    
}

function boutonsChronoOn(typeChrono) {
	var xhttp = new XMLHttpRequest();
	var idBtn;
	var typeAutre;
	if(typeChrono == 'court') {idBtn = 'btnCourt';  typeAutre='long';}
	if(typeChrono == 'long') {idBtn = 'btnLong'; typeAutre = 'court';}
	
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById(idBtn).innerHTML = this.responseText;
		}
	};

	xhttp.open("GET", "ajax/btnChronoOn.ajax.php?typeChrono= "+typeChrono, true);
	xhttp.send();
	
  boutonsChronoOff(typeAutre);
}


function boutonsChronoOff(typeChrono) {
	var xhttp = new XMLHttpRequest();
	var idBtn;
	if(typeChrono == 'court') idBtn = 'btnCourt';
	if(typeChrono == 'long') idBtn = 'btnLong';
	
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById(idBtn).innerHTML = this.responseText;
		}
	};

	xhttp.open("GET", "ajax/btnChronoOff.ajax.php?typeChrono= "+typeChrono, true);
	xhttp.send();
}

function boutonsChronoPause(typeChrono) {
	var xhttp = new XMLHttpRequest();
	var idBtn;
	var etat;
	if(typeChrono == 'court') 
	{
		idBtn = 'btnCourt';
		etat = document.getElementById('chrono_court').innerHTML
	}
	if(typeChrono == 'long') 
   {	
		idBtn = 'btnLong';
		etat = document.getElementById('chrono_long').innerHTML
	}

	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById(idBtn).innerHTML = this.responseText;
		}
	};

	xhttp.open("GET", "ajax/btnChronoPause.ajax.php?typeChrono= "+typeChrono +"&etat="+etat, true);
	xhttp.send();
}