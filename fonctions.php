<?php 
/*Le Procrastinateur -- 29/08/2020
 *
 * fonctions.php
 * Auteur : Acryline Erin
 * Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 3.0 France.
 * 
 */
 

// ==============================================================
// Validation des données entrées
// ============================================================== 
 function valid_donnees($donnees)
 {
  $donnees = str_replace('"', '´´', $donnees);	
  $donnees = str_replace("'", "´", $donnees);	
  $donnees = str_replace(">", " ", $donnees);
  $donnees = str_replace("<", " ", $donnees);
  $donnees = str_replace("&", "et", $donnees);
  $donnees = strip_tags($donnees); 
  $donnees = trim($donnees); 
  $donnees = stripslashes($donnees); 
  $donnees = htmlspecialchars($donnees,ENT_QUOTES,'UTF-8');  
  return $donnees;
 }
   
// ==============================================================
// Charger les émojis
// ============================================================== 
function chargerEmojis() 
{
	$tabEmoji = array();
	$resultat = file('./doc/emoji.txt');
	$resultat= array_merge($resultat, file('./doc/emoji_nature.txt'));
	$resultat= array_merge($resultat, file('./doc/emoji_smiley.txt'));	
	$resultat= array_merge($resultat, file('./doc/emoji_geste.txt'));
	$resultat= array_merge($resultat, file('./doc/emoji_emotion.txt'));
	$resultat= array_merge($resultat, file('./doc/emoji_vetement.txt'));
	$resultat= array_merge($resultat, file('./doc/emoji_aliment.txt'));
	$resultat= array_merge($resultat, file('./doc/emoji_objet.txt'));
	$resultat= array_merge($resultat, file('./doc/emoji_symbole.txt'));
	$nbr = count($resultat);
	for($i = 0;$i<$nbr;$i++)
	{
     $tab = explode(",",$resultat[$i]);
     $tabEmoji[trim($tab[1])]=$tab[0];
	}
	return $tabEmoji; 
}

// ==============================================================
// Trouver le nom d'un émoji
// ==============================================================
function trouverNomEmoji($emoji) 
{
	 $nomEmoji ='';
	 foreach($_SESSION['emoji'] as $nom => $emo)
	 {
      if($emo == $emoji) return $nom; 
	 }
	 return '';
}

// ==============================================================
// Affichage de la sélection  d'émoji
// ============================================================== 
function selectEmoji($selectCle, $idEmoji)
{
  $nbrEmojis = 0;
  $tabEmoji = array();
  $i = 0;
 
  if(isset($_SESSION['emoji']))
  { 
  		$nbrEmojis = count($_SESSION['emoji']);
  		$tabEmoji = $_SESSION['emoji'];
  }
  if($selectCle)
  {
  	 $emoji = $_SESSION['emoji'][$selectCle];
  }else {
  	 $emoji = '🦋';
  }
 ?> 
  <div class="w3-dropdown-hover  w3-white ">
  <br><button class="w3-button w3-circle w3-xxlarge w3-white w3-hover-lime w3-light-grey"  style='font-family:initial;' disabled='disabled' id="<?php echo $idEmoji; ?>" > 
  		<?php echo $emoji; ?> 
  		<input type='hidden' name='emoji' value=' <?php echo  $selectCle ; ?>' />
  </button>
  <div class='w3-dropdown-content w3-bar-block w3-border w3-round selectCadre w3-light-grey'  >
  <div class="w3-label w3-lime"> Selection Minute Papillon </div>
 <?php
  foreach ( $tabEmoji as $cle => $emoji)
  {
  	 	echo "<span class='w3-xlarge w3-white emoji' name ='".$i."' onclick ='selectEmoji(".$i.",".$idEmoji.")' >".$emoji."</span>";
  	 	if ($i == 33) echo "<br><div class='w3-label w3-lime'> Animaux et plantes </div>";
  	 	if ($i == 151) echo "<br><div class='w3-label w3-lime'> Smileys </div>";
  	 	if ($i == 257) echo "<br><div class='w3-label w3-lime'> Corps et gestes </div>";
  	 	if ($i == 298) echo "<br><div class='w3-label w3-lime'> Émotions </div>";
  	 	if ($i == 329) echo "<br><div class='w3-label w3-lime'> Vêtements et accessoires </div>";
  	 	if ($i == 362) echo "<br><div class='w3-label w3-lime'> Aliments et boissons </div>";
  	 	if ($i == 461) echo "<br><div class='w3-label w3-lime'> Objets </div>";
		if ($i == 635) echo "<br><div class='w3-label w3-lime'> Symboles </div>";
      $i++;
  }
  echo "</div>";
  echo "</div>";
}

// ==============================================================
// Charger les premières taches
// ============================================================== 
function  chargerPremiereTache($tabTaches,$type)
{
  $tab = array();
  $tab['tache']="Enregistrez vos tâches depuis le menu Gestion.";
  $tab['emoji']= 'inexistant';
  $nbrTaches = 0;
  if(is_array($tabTaches))
  {
  		$nbrTaches = count($tabTaches);
  }
  	for($i=0;$i<$nbrTaches;$i++) 
  	{
  		if(isset($tabTaches[$i])) 
  		{	  
  	  		if($tabTaches[$i]['type_liste'] == $type && $tabTaches[$i]['statut'] == 'actif')
  	  		{
  	  			$tab = $tabTaches[$i];
  	  	 		return $tab;
  	  		}
		}
  	}
  return $tab;
}

// ==============================================================
// Afficher le cadre de création ou de modification des tâches
// ============================================================== 
function afficherTache()
{
 ?>
     <label>Intitulé : </label>
	 	<input class='w3-input' type='text' name='nouvelle_tache' />
	 	<div class="w3-row-padding ">
	 		<div class="w3-third ">
	 		  <?php selectEmoji("","save");?>
	 		</div>
		 	<div class="w3-third ">
			 	<label>Temps des séances : </label><br>
			 	<label>Court </label>
			 	<input class="" type="radio" name="type_liste" value="courte" checked>&nbsp;&nbsp;
				<label>Long</label>	
				<input class="" type="radio" name="type_liste" value="longue"><br>	
			</div>
		 	<div class="w3-third ">
			 	<label>Statut de la tâche : </label><br>
			 	<label>Actif </label>
			 	<input class="" type="radio" name="statut" value="actif" checked>&nbsp;&nbsp;
				<label>Archivé</label>	
				<input class="" type="radio" name="statut" value="archive"><br>	
			</div>					
		</div><br>	
		<input class='w3-btn w3-lime' type='submit' name='enregistrer_tache' value='Enregistrer'/>
     <br><br>
<?php
}

// ==============================================================
// Afficher le filtre de sélection des tâches
// ============================================================== 
function filtreTache()
{
 ?>
  <div class="w3-row" id="filtre">
	  <div class="w3-third">  
	  FILTRE DES TÂCHES ? 
	  </div>
	  <div class="w3-third">
				<input class="" type="radio" name="tri_type" value="tout" onclick='filtre();' checked='checked'>
				<label>Courtes et longues</label><br>		
			 	<input class="" type="radio" name="tri_type" value="courte"  onclick='filtre();'>
			 	<label>Courtes </label><br>
				<input class="" type="radio" name="tri_type" value="longue" onclick='filtre();'>
				<label>Longues</label>				
     </div>
	  <div class="w3-third"> 
				<input class="" type="radio" name="tri_statut" value="tout" onclick='filtre();' checked='checked'>	  
				<label>Actives et archivées</label>	<br>
			 	<input class="" type="radio" name="tri_statut" value="actif" onclick='filtre();'>
			 	<label>Actives </label><br>
				<input class="" type="radio" name="tri_statut" value="archive" onclick='filtre();'>
				<label>Archivées</label>
	  </div>
  </div> <br>

 <?php 
}

// ==============================================================
// Afficher le cadre de création ou de modification des tâches
// ============================================================== 
function afficherTacheModif($infos)
{
 ?>
     <label>Intitulé : </label>
      <input type='hidden' name='id_tache' value = '<?php echo $infos['id'];?>'/>
	 	<input class='w3-input' type='text' name='modif_tache' value = '<?php echo htmlspecialchars($infos['tache'],ENT_QUOTES,'UTF-8');?>'/>
	 	<div class="w3-row-padding">
	 		<div class="w3-third ">
	 		  <?php selectEmoji($infos['emoji'], "Modif")?>
	 		</div>
		 	<div class="w3-third ">
			 	<label>Temps des séances : </label><br>
			 	<?php
			 	if($infos['type'] == "courte")
			 	{
				 	echo "<label>Court </label>";
				 	echo "<input type='radio' name='type_liste' value='courte' checked>&nbsp;&nbsp;";
					echo "<label>Long</label>";	
					echo "<input  type='radio' name='type_liste' value='longue'><br>";
			   }
			   elseif($infos['type'] == "longue") 
			   {
				 	echo "<label>Court </label>";
				 	echo "<input type='radio' name='type_liste' value='courte' >&nbsp;&nbsp;";
					echo "<label>Long</label>";	
					echo "<input  type='radio' name='type_liste' value='longue' checked><br>";			   	
			   }	
				?>
			</div>
		 	<div class="w3-third ">
			 	<label>Statut de la tâche : </label><br>
			 	<?php
			 	if($infos['etat'] == "actif")
			 	 {
				 	echo "<label>Actif </label>";
				 	echo "<input  type='radio' name='statut' value='actif' checked>&nbsp;&nbsp;";
					echo "<label>Archivé</label>";	
					echo "<input  type='radio' name='statut' value='archive'><br>";
				 }
				 elseif($infos['etat'] == "archive")
				 {
				 	echo "<label>Actif </label>";
				 	echo "<input  type='radio' name='statut' value='actif' >&nbsp;&nbsp;";
					echo "<label>Archivé</label>";	
					echo "<input  type='radio' name='statut' value='archive' checked><br>";				 
				 } 
				?>	
			</div>					
		</div><br>	
		<input class='w3-btn w3-lime' type='submit' name='modifier_tache' value='Modifier'/>
		<input class='w3-btn w3-orange' type='submit' name='supprimer_tache' value='Supprimer'/>
     <br><br>
<?php
}

// ==============================================================
// Afficher le tableau des tâches
// ============================================================== 
function afficherTableau($tabTaches, $filtre_type, $filtre_statut)
{
	$nbrTaches=0;
	if(is_array($tabTaches)) 
	{
		$nbrTaches = count($tabTaches);
	}	
   ?>
   <table class="w3-table w3-striped">
      <tr class="w3-lime">
	      <th>Emoji</th>
	      <th>Tâche</th>
	      <th>Type</th>
	      <th>Etat</th>
	      <th>Choix</th>
	    </tr>
   <?php	
   if($nbrTaches>0)
   {		   
	   for($i=0;$i<$nbrTaches;$i++)
	   {
	   	if(isset($_SESSION['emoji'][$tabTaches[$i]['emoji']]) && isset($tabTaches[$i]['tache'] ) 
	   	 && isset($tabTaches[$i]['type_liste'] ) && isset($tabTaches[$i]['statut'] ))
	   	{
		   	$emoji = $_SESSION['emoji'][$tabTaches[$i]['emoji']]; 
		   	echo "<tr id='ligneTache'>";
		   	echo "<td class='w3-xlarge' style='font-family:initial;'>".$emoji."</td>";
		   	echo "<td>".$tabTaches[$i]['tache']."</td>";
		   	echo "<td>".$tabTaches[$i]['type_liste']."</td>";
		   	echo "<td>".$tabTaches[$i]['statut']."</td>";
		   	echo "<td><input class='radio' type='radio' name='selec' value='$i' onclick='afficherSelection();'/></td>";
		   	echo "</tr>";
	      }
	   }
   }else{
   	echo "<tr ><td  colspan='5'>Aucune tâche enregistrée.</td></tr>";
   	echo "<tr ><td  colspan='5'></td></tr>";
   }
   echo "</table>";
}

// ==============================================================
// Afficher la partie de la gestion des tâches
// ============================================================== 
function afficherTabTaches($tabTaches) 
{
   $nbrTaches=0;
   $infosTache = array('id'=> NULL, 'tache'=> '', 'emoji' => '','type'=> 'courte', 'etat' => 'actif');
	?>

	<div class="w3-margin-left w3-margin-right  w3-white">
	<div class='w3-panel w3-lime w3-card-2'><b>GESTION DES TÂCHES</b></div>
	<form class='w3-margin' action="gestion.php" method="POST" >
	<div class="w3-lime w3-panel w3-padding w3-large">Sélectionnez une tâche pour la modifier ou la supprimer.  </div>
	<div id="modifTache"><?php afficherTacheModif($infosTache); ?></div>	
	<div><?php filtreTache();?></div>
	<div id="tableau_taches">
   <?php afficherTableau($tabTaches, 'tout', 'tout'); ?>
   </div>
 		</form>
		</div>
	<?php
}


// ==============================================================
// Affichage de l'outil d'enregistrement de nouvelle tache
// ============================================================== 
function nouvelleTache($message)
{
	echo "<div class='w3-margin-left w3-margin-right  w3-white'>";
	echo "<div class='w3-panel w3-lime w3-card-2'> <b>ENTRER UNE NOUVELLE TÂCHE</b> </div>";
   echo "<form class='w3-margin' action='gestion.php' method='POST'>";
			afficherTache(); 
   echo "</form>";
 	if(isset($message)) 
	 	{
			  echo  $message;			 	
	 	}else {echo "<br>";} 
 	echo "</div><br>";	
}

// ==============================================================
// Affichage  de l'outil de sauvegarde des listes
// ==============================================================
function sauvegarderListes()
{
	?>
   <div class="w3-margin-left w3-margin-right w3-white">
   <div class='w3-panel w3-lime w3-card-2'><b> SAUVEGARDER LES LISTES</b></div>
	 <form class='w3-margin' action="gestion.php" method="POST">
	 	<div class="w3-row">
		 	<div class="w3-half ">
			 	<label>Temps des séances : </label><br>
			 	<label> Court </label>
			 	<input class="" type="radio" name="type_liste" value="courte" checked>&nbsp;&nbsp;
				<label> Long</label>	
				<input class="" type="radio" name="type_liste" value="longue">	
				<label> &nbsp;Tout</label>	
				<input class="" type="radio" name="type_liste" value="tout"><br>							
			</div>
		 	<div class="w3-half ">
			 	<label>Statut de la tâche : </label><br>
			 	<label> Actif </label>
			 	<input class="" type="radio" name="statut" value="actif" checked>&nbsp;&nbsp;
				<label> Archivé</label>	
				<input class="" type="radio" name="statut" value="archive">	
				<label> &nbsp;Tout</label>	
				<input class="" type="radio" name="statut" value="tout"><br>							
			</div>					
		</div><br>		
	 		<input class='w3-btn w3-lime' type='submit' name='save_liste' value='Sauvegarder'/>
	 </form><br>	
 		 <?php 
		 	if(isset($message['init'])) 
		 	{
           echo  $message['init'];			 	
		 	} 
    	echo "</div><br>";
}

// ==============================================================
// Affichage  de l'outil de restauration des listes
// ==============================================================
function restaurerListes()
{
	?>
	<div class="w3-margin-left w3-margin-right w3-white">
	<div class='w3-panel w3-lime w3-card-2'><b> RESTAURER DES LISTES</b></div>
	 <form class='w3-margin' action="gestion.php" method="POST">	
	 <input class='w3-btn w3-lime' type='submit' name='save_liste' value='Restaurer'/>
	 </form><br>
	</div><br>
   <?php
}

// ==============================================================
// Affichage  de l'outil de suppression des données
// ==============================================================
function purgerDonnees() 
{
 ?>
	   	<div class="w3-margin-left w3-margin-right  w3-white" >
	   	<div class='w3-panel w3-lime w3-card-2'> </div>
	   	<div class='w3-margin w3-center '>
		 	<b><button class='w3-btn  w3-orange  w3-margin-top w3-padding' onclick="purgerData();" >RÉINITIALISER TOUTES LES DONNÉES ET VIDER LES LISTES </button></b>
	   	</div>
 		 	<?php 
		 		if(isset($message['init'])) 
		 		{
           			echo  $message['init'];			 	
		 		}	 
		 	?>
		<br></div>
<?php
}

// ==============================================================
// Affichage  de l'outil de gestion des chronos
// ==============================================================
function gestionChronos($tpsCourt,$tpsLong)
{
	?>
   <div class="w3-margin-left w3-margin-right w3-white">
   <div class='w3-panel w3-lime w3-card-2'> <b>GESTION DES CHRONOMÈTRES</b></div>
   <div class="w3-row">
   <div class="w3-quarter logochrono">
   	<span>⏱</span>
   </div>
   <div class="w3-threequarter">
	<form class='w3-margin w3-large' action="gestion.php" method="POST">	
	   <label>Temps d'une tâche courte : </label>
	   <input class='chrono lobster' type='number' name='tps_court'  size ='5'/ value='<?php echo $tpsCourt;?>'> minutes<br><br>
	   <label>Temps d'une tâche longue : </label>
	   <input class='chrono lobster' type='number' name='tps_long'  size ='5'/ value='<?php echo $tpsLong;?>'> minutes<br><br>
	   <input class='w3-btn w3-lime' type='submit' name='enregistrer_chrono' value='Enregistrer'/>
	   <br><br>
   </form>
	 <?php 
	 	if(isset($message['timers'])) 
	 	{
        echo  $message['timers'];			 	
	 	} 
	echo "</div>"; 	
	echo "</div>";
   echo "</div>";
}


// ==============================================================
// Enregistrer une nouvelle tâche
// ============================================================== 
function enregistrerTache($infos) 
{
	if(strlen($infos['nouvelle_tache']) == 0)
	{
		return html_entity_decode("<span class='w3-padding w3-text-red'>Veuillez entrer une tâche.</span>");
	}
   $tabTache = array();	
   $nbrTaches = 0;
	if(isset($_SESSION['Liste']))
	{
     	$tabTache = json_decode($_SESSION['Liste'],TRUE);
     	if(is_array($tabTache)) {$nbrTaches = count($tabTache);}
     	else {
   		$tabTache = array();	
   		$nbrTaches = 0;     		
     	}
	}
  
	$tabTache[$nbrTaches]['tache'] = valid_donnees($infos['nouvelle_tache']);
	$tabTache[$nbrTaches]['emoji'] = $infos['emoji'];
	$tabTache[$nbrTaches]['type_liste'] = $infos['type_liste'];
	$tabTache[$nbrTaches]['statut'] = $infos['statut'];
	
	$json = json_encode ($tabTache);
	if($json)  echo "<script > localStorage.setItem('liste',JSON.stringify(".$json.")); </script>";
	$_SESSION['Liste'] = $json;

	return html_entity_decode("<span class='w3-padding w3-text-black'>La tâche est enregistrée.</span>");
}

// ==============================================================
// Enregistrer de nouvelles valeurs pour les timers 
// ============================================================== 
function enregistrerTimers($infos) 
{
	if($infos['tps_court'] == 0 || $infos['tps_long'] == 0)
	{
		return html_entity_decode("<span class='w3-padding w3-text-red'>Le temps d'une tâche  ne peux pas être égal à 0.</span>");
	}
	$tpsCourt = intval($infos['tps_court'])*60;
	$tpsLong = intval($infos['tps_long'])*60;
	echo "<script > localStorage.setItem('timerCourt',$tpsCourt ); </script>";
	echo "<script > localStorage.setItem('timerCourtActif',$tpsCourt ); </script>";
	echo "<script > localStorage.setItem('timerLong',$tpsLong ); </script>";
	echo "<script > localStorage.setItem('timerLongActif',$tpsLong ); </script>";
	$_SESSION['tpsCourt'] = $infos['tps_court'];
	$_SESSION['tpsLong'] = $infos['tps_long'];
	return html_entity_decode("<span class='w3-padding w3-text-black'>Le temps des chronomètres sont modifiés.</span>");
}

// ==============================================================
// Réinitialiser les données 
// ============================================================== 
function initialiserData($infos)
{
  echo "<script > localStorage.setItem('liste',JSON.stringify('{}')); </script>";
  echo "<script > localStorage.setItem('timerCourt',480 ); </script>";
  echo "<script > localStorage.setItem('timerCourtActif',480 ); </script>";
  echo "<script > localStorage.setItem('timerLong', 2400); </script>";
  echo "<script > localStorage.setItem('timerLongActif',2400 ); </script>";
  $_SESSION['Liste']='{}';
  $_SESSION['tpsCourt'] = 8;
  $_SESSION['tpsCourtActif'] = 8;
  $_SESSION['tpsLong'] = 40;
  $_SESSION['tpsLongActif'] = 40;		
  return html_entity_decode("<span class='w3-padding w3-text-black'>Données réinitialisées.</span>"); 
}

// ==============================================================
// Supprimer une tâche
// ============================================================== 
function supprimerTache($infos) 
{
   $tab = array();
   $tabTaches = json_decode($_SESSION['Liste'],TRUE);
   $nbr = count($tabTaches);
   $id = $infos['id_tache'];
   for($i=0;$i<$nbr;$i++)
    {
    	if($i != $id) array_push($tab,$tabTaches[$i]);
    }
	$json = json_encode ($tab);
	if($json)  echo "<script > localStorage.setItem('liste',JSON.stringify(".$json.")); </script>";
	$_SESSION['Liste'] = $json;
}

// ==============================================================
// Modifier une tâche
// ============================================================== 
function modifierTache($infos) 
{
	$tabTaches = json_decode($_SESSION['Liste'],TRUE);
   $nbr = count($tabTaches);
   $id = $infos['id_tache'];

   for($i=0;$i<$nbr;$i++)
    {
    	if($i == $id)
    	{
			$tabTaches[$i]['tache'] = valid_donnees($infos['modif_tache']);
			$tabTaches[$i]['emoji'] = trim($infos['emoji']);
			$tabTaches[$i]['type_liste'] = $infos['type_liste'];
			$tabTaches[$i]['statut'] = $infos['statut'];
    	}
    }
	$json = json_encode ($tabTaches);
	if($json)  echo "<script > localStorage.setItem('liste',JSON.stringify(".$json.")); </script>";
	$_SESSION['Liste'] = $json;    
}
