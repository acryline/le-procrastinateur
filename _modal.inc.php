<aside id="modal1" class="modal w3-small" aria-hidden="true" aria-labelledby="titre-modal" style="display:none;" > 
	<div class="w3-round modal-wrapper w3-display-container" role ="dialog" >
	    <div class="w3-xlarge" style="padding-left:20px;  text-align: justify;" id="titre-modal">Informations</div>
	    <button class="w3-btn w3-display-topright" id='fermer_croix' type="button"  onclick="fermerModal();">X</button>
	      </p>À l'origine, <b>Minute Papillon</b> a été développé pour un usage personnel. 
	      Le but n'était pas de le publier. Toutefois, il est mis à disposition de ceux qui voudraient le tester. 
	      Il correspondra  et  sera peut-être utile à certains d'entre-vous ?</p>
	      <p> Minute Papillon respecte à 100% votre vie privée côté serveur :
         <ul class="w3-ul">	      
	      	<li>💚 pas d'enregistrement, donc pas d'informations personnelles,</li>
	      	<li>💚 pas de base de données, en dehors des logs liés au fonctionnement du serveur, aucune information de connexion n'est enregistrée,</li>
	      	<li>💚 toutes  les données que vous entrez sur le site restent chez vous dans votre navigateur,</li>
	      	<li>💚 donc, vos données ne peuvent pas être interceptées via le réseau,
	      	<li>💚 Le code du site est libre est peut être consulté 
	      	<a href="https://framagit.org/acryline/le-procrastinateur/" >à cette adresse.</a> </li> 
	      </ul> 
	      En revanche, attention, dans l'état actuel de développement du site il n'est pas 
	      possible de sauvegarder (facilement) vos tâches et vous pourrez les perdre si :
	      <ul class="w3-ul">
	        	<li>❗️vous videz le cache de votre navigateur,</li>
	      	<li> ❗️ ou si vous réinstallez votre système d'exploitation ou votre navigateur.</li>
	      	<li> ❗️De plus, vos tâches pourraient devenir obsolètes après une mise à jour majeure du site.</li>
	      	<li> ❗️Attention : côté client, donc chez vous, les données ne sont pas chiffrées. 
	      	Elles sont donc accessibles facilement à tout utilisateur ayant accès votre navigateur, et modifiables de la même façon. </li>
         </ul>	      
	      </p>
	      <p> Bonne visite et merci d'essayer  <b> Minute Papillon</b> !</p>
	      <p class="w3-tiny">L’éditeur s’efforce de permettre l’accès au site 24 heures sur 24, 7 jours sur 7, sauf en cas de force majeure ou 
	      d’un événement hors du contrôle de l’EDITEUR, et sous réserve des éventuelles pannes et interventions de maintenance 
	      nécessaires au bon fonctionnement du site et des services. <br>
	      L’EDITEUR ne peut garantir une disponibilité du site et/ou des services, une fiabilité des transmissions 
	      et des performances en terme de temps de réponse ou de qualité. Il n’est prévu aucune assistance technique 
	      vis à vis de l’utilisateur que ce soit par des moyens électronique ou téléphonique.
         La responsabilité de l’éditeur ne saurait être engagée en cas d’impossibilité d’accès à ce site et/ou d’utilisation des services.
         Par ailleurs, l’EDITEUR peut être amené à interrompre le site ou une partie des services, à tout moment sans préavis,
         le tout sans droit à indemnités. L’utilisateur reconnaît et accepte que l’EDITEUR ne soit pas responsable des interruptions 
         et des pertes de données, et des conséquences qui peuvent en découler pour l’utilisateur ou tout tiers. </p>
         <input class="w3-check" type="checkbox" id="fermer_def"> <label>Ne plus ouvrir cette fenêtre.</label>	
    	   <button class=" w3-btn w3-lime w3-margin" type="button" id="fermer" onclick="fermerModal();" >Fermer</button><br/><br/>
	</div>
</aside>