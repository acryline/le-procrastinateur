<?php
session_start();
include ('fonctions.php');
$infos = array();
if(isset($_GET['id']) && isset($_GET['emoji']) && isset($_GET['tache']) && isset($_GET['typeTache'])  && isset($_GET['etat']))
{
	$infos['id'] = $_GET['id'];
	$nomEmoji = trouverNomEmoji($_GET['emoji']);
   $infos['emoji'] =  trim($nomEmoji);
   $infos['tache'] = $_GET['tache'];
   $infos['type'] = $_GET['typeTache'];
   $infos['etat'] = $_GET['etat'];
   afficherTacheModif($infos);   
}
?>