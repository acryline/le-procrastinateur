<?php
/*Le Procrastinateur -- 06/09/2020
 *
 * index.php
 * Auteur : Acryline Erin
 * Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 3.0 France.
 * 
 */
include("_header.inc.php");
$tabCourt = array();
$tabLong = array();
if (!$tabTaches) 
{	
	$tabCourt['tache'] = "Enregistrez vos tâches depuis le menu Gestion.";
	$tabCourt['emoji'] = 'inexistant';
	$tabLong['tache']= $tabCourt['tache'];
	$tabLong['emoji'] =$tabCourt['emoji'];
}else {
	$tabCourt = chargerPremiereTache($tabTaches,"courte");
	$tabLong = chargerPremiereTache($tabTaches,"longue");
}  
$iconeCourt = $_SESSION['emoji'][trim($tabCourt['emoji'])];
$iconeLong = $_SESSION['emoji'][trim($tabLong['emoji'])];

?>

<div class="w3-row">
  	<div class="w3-half "><br>	
 		<div class="w3-margin-left w3-margin-right w3-light-gray"><br>	
  	  	<label class="w3-padding w3-white w3-margin-left">Tâches courtes</label>
  	  	<span id="btnCourt">
  	  		<label class="w3-padding w3-white w3-margin-left" id="chrono_court">00 h : 00 mn : 00 s </label>
	  	  	&nbsp;&nbsp;&nbsp;<button class="w3-btn w3-green" id="btn-go-court" onclick="lancerChrono('court');">Démarrer</button><br><br>
  	  	</span>
  	  	<div class="w3-center" style="font-size:10em; font-family:initial;"><?php echo $iconeCourt ;?></div><br>
  	  	<div class="w3-padding w3-center w3-margin-left w3-large lobster w3-xxlarge "  style="width:95%;"><?php echo $tabCourt['tache'];?></div><br>
		<button class="w3-btn w3-green w3-margin-left" id="btn_retour_court" onclick="retourTache('courte');">Retour</button>	
		<button class="w3-btn w3-green w3-margin-left" id="btn_passer_court" onclick="passerTache('courte');">Passer</button>
		<button class="w3-btn w3-green w3-margin-left" id="btn_archiver_court" onclick="archiverTache('courte');">Archiver</button><br><br>
  		</div>
  	</div>
  	<div class="w3-half"><br>	
  	  <div class="w3-margin-left w3-margin-right w3-light-gray"><br>	
  	  	<label class="w3-padding w3-white w3-margin-left">Tâches longues</label>
  	  	<span id="btnLong">
	  	  	<label class="w3-padding w3-white w3-margin-left" id="chrono_long">00 h : 00 mn : 00 s</label>
  	  		&nbsp;&nbsp;&nbsp;<button class="w3-btn w3-green" id="btn-go-long" onclick="lancerChrono('long');"> Démarrer</button><br><br>
  	  	</span>
  	  	<div class="w3-center" style="font-size:10em; font-family:initial;"><?php echo $iconeLong;?></div><br>
  	  	<div class="w3-padding w3-center w3-margin-left w3-large lobster w3-xxlarge "  style="width:95%;"><?php echo $tabLong['tache'];?></div><br>
		<button class="w3-btn w3-green w3-margin-left" id="btn_retour_long" onclick="retourTache('longue');">Retour</button>	
		<button class="w3-btn w3-green w3-margin-left" id="btn_passer_long" onclick="passerTache('longue');">Passer</button>
		<button class="w3-btn w3-green w3-margin-left" id="btn_archiver_long" onclick="archiverTache('longue');">Archiver</button><br><br>
  	  </div>
  	</div>	  	
</div>
	

<?php
include("_footer.inc.php");
?>