# Minute Papillon
Site de gestion de tâches pour Procrastinateur.

[Démonstration](https://cogito.no-ip.info/cogito/papillon/index.php)

## Organisation générale

#### Protection des données : 
Toutes les données entrées depuis les pages du site **Minute Papillon** restent chez l'utilisateur. Aucune information n'est stockée sur le serveur du site **Minute Papillon**

#### Types d'action sur le client 
* Configurer des options : création et modification de listes,  purger les informations enregistrées, configurer des options.
* Utiliser des listes circulaires : principe des tâches courtes et longues, système du temps court etc. 
* Trouver  des informations sur le fonctionnement et les principes du site : Aide, FAQ.
* consulter les informations légales du site.

## Installation 

* Charger la source:

`git clone https://framagit.org/acryline/le-procratinateur.git
` 

* Mise à jour :

`git pull https://framagit.org/acryline/le-procrastinateur.git
`

## Prochaines implémentations
* Afficher et activer les boutons  Déplacer, en arrière.
* Sauvegarder une liste.
* Restaurer une liste.
* Tutoriel 

## Améliorations :

* Ajouter des catégories
* Timer aléatoires
* Programmes de timers
* Utilisation de base de données locales.
* Mémoriser l'état de la tâche / organisation / matériel /idées pour installation rapide à la reprise.
* Possibilité d'écrire des remarques ou un bilan




