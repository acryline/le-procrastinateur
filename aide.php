<?php
/*Minute Papillon -- 06/09/2020
 *
 * aide.php
 * Auteur : Acryline Erin
 * Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 3.0 France.
 * 
 */

include("_header.inc.php");
?>
	  <div class="w3-row-padding w3-margin w3-white aide ">
	  		<div class='w3-half'><br>
					<button onclick="accordion('sujet0')" class="w3-button w3-block w3-left-align w3-large w3-padding-24 w3-lime w3-card-2 w3-round"><b>🦋 Pourquoi une ennième application <br>de liste de tâches ?</b></button>
					<div id="sujet0" class="w3-container w3-hide ">
					  <p>
					  Deux raisons principales :
					  <ul>
					  <li><b>La différence</b> : nous ne fonctionnons pas tous et tous les jours de la même façon.</li>
					  <li><b>La liberté</b> : en général, les utilisateurs d'outils numériques doivent se plier au fonctionnement des logiciels
					  alors que, ce sont les logiciels qui devraient se plier aux besoins des utilisateurs.
					  L'illusion de rentabilité, les habitudes de certains gourous, l'histoire,  la généalogie et l'héritage des codes, 
					  les préjugés et la culture très technique et peu humaine ou sociale  des développeurs influence beaucoup 
					  trop les nouveaux outils numériques et oblige l'humain à fonctionner comme une machine.</li> 
					  </ul>
					  </p>
					  <p>
					  À la base ce site a été développé pour un usage personnel et local, il ne devait pas être publié.
					  Cependant, je le  mets à la disposition de ceux qui veulent le tester. Il correspondra  peut-être à vos besoins. 
					  </p>
					</div><br>
							  		
					<button onclick="accordion('sujet1')" class="w3-button w3-block w3-left-align w3-large w3-padding-24 w3-lime w3-card-2 w3-round"><b>🦋 Pourquoi un papillon ?</b></button>
					<div id="sujet1" class="w3-container w3-hide">
					  <p> <strong>Un papillon</strong> est libre de virevolter de fleur en fleur au gré de son humeur et de ses besoins et butiner le nectar et le pollen. Nous n'irons pas plus loin dans
					  la description bucolique du papillon. Mais, l'image est assez parlante pour expliquer la nature de ce site.</p>
					  <p> Pour la comparaison, comme le papillon, nous pouvons passer d'une idée ou d'une envie à l'autre, en partir puis, y revenir. Nous avons besoin de nous nourrir, nous distraire, nous reposer, construire,
					  apprendre, tester, imaginer, digérer, analyser. "Fabriquer/faire" ne devrait être qu'une partie de notre activité.</p>   
					  <p> Nous n'avançons pas en ligne droite. Nous devons virevolter d'idée en idée de besoin en besoin. <strong>La ligne droite est souvent le plus court  chemin pour perdre son temps et son âme.</strong>
                 <br>En prenant en compte notre non linéarité, en choisissant notre route  en fonction de nos besoins, nous avançons souvent plus vite et mieux que sur un itinéraire tracé contre-nature.</p>
					</div><br>	
					
					<button onclick="accordion('sujet2')" class="w3-button w3-block w3-left-align w3-large w3-padding-24 w3-lime w3-card-2 w3-round"><b> 🦋 Description de la démarche</b></button>
					<div id="sujet2" class="w3-container w3-hide">
					  <p><strong>Il n'y a pas de démarche.</strong> </p>
					  <p><strong>Il y a deux principes : le local et la boucle. </strong> 
					  <ul>
					  <li><strong>Le local</strong> pour limiter la portée d'une action et maîtriser ses limites afin qu'elle soit moins stressante, plus adaptée,
					  moins contraignante, plus abordable et tout simplement moins longue. </li>
					  <li><strong>La boucle</strong> pour aborder une tâche plusieurs fois, l'apprivoiser, l'assimiler, la comprendre et pour diversifier les tâches, alterner, 
					  s'adapter à nos besoins et à l'état d'esprit d'un moment, au degré de notre concentration et de notre endurance à l'effort physique ou intellectuel.</li> 
					  </ul><br> 
					  <strong>Sur la page d'accueil</strong>, la première tâche courte active et la première tâche longue active sont affichées.  
					  Vous pouvez démarrer une de ces deux tâches en déclenchant le compte à rebours, la passer ou l'archiver.
					  <ul> 
					  <li><strong>Quand le décompte du temps se termine</strong>, une sonnerie est jouée puis la tâche courte ou longue suivante s'affiche et la tâche réalisée est placée en fin de liste. </li>
					  <li>Pareil lorsque vous <strong>passez la tâche</strong>.</li>
					  <li>Quand vous <strong> archivez une tâche </strong> elle sort de la boucle. Vous pouvez la réactiver depuis la page "Gestion".</li>
					  <li>Une idée pour améliorer le site serait d'ajouter un bouton "arrière" et permettre ainsi de prolonger plus facilement la tâche. Mais, 
					  on peut aussi prolonger le temps depuis la page "Gestion".</li>
                 </ul>					  
					  </p>
					</div><br>
					
    		</div>
	  		<div class='w3-half'><br>
					<button onclick="accordion('sujet4')" class="w3-button w3-block w3-left-align w3-large w3-padding-24 w3-lime w3-card-2  w3-round"><b> 🦋 Exemple d'utilisation</b></button>
					<div id="sujet4" class="w3-container w3-hide">
					  <p>La souplesse de cet outil numérique est à la disposition de l'utilisateur pour qu'il en fasse ce qu'il veut<strong> en fonction de ses besoins, de ses envies, de ses obligations et de 
					  ses états d'âmes. </strong>
					  <ul>
						<li> vous devez faire <strong>un travail pour le lendemain</strong> ? Vous n'avez pas besoin de ce logiciel, vous savez déjà ce que vous avez à faire.
						 Ou alors, découpez votre travail en sous-tâches plus digestes. Mais vous devrez tout de même avancer de façon linéaire.</li> 
						<li> vous avez<strong> un besoin de ligne droite</strong> ? Faites une liste de tâches longues que vous suivrez rigoureusement,</li> 
						<li> vous avez <strong>une multitude de choses pénibles</strong> à faire, et pas du tout envie de les  faire ? Utilisez la liste de tâches courtes 
						en boucle. À raison de 10 minutes par tâche, fini ou pas fini, passez à autre chose, la journée sera peut-être plus digeste ? </li> 	
						<li> <strong>une activité que vous aimez vous prend trop de temps</strong> et vous négligez le reste ? Alternez tâche longue (ce que vous aimez faire) et tâche courte (ce que vous n'aimez pas).</li> 
						<li> <strong>réglez les temps court et long </strong>comme vous le voulez.</li> 
						<li> <strong>vous avez un travail administratif à faire</strong>, mais ce n'est vraiment pas votre truc ? 5 minutes par heure, après un épisode de votre série préférée, le bain des gamins, la balade du chien,
						... vous verrez à la fin de la journée vous en serez venu à bout. Un conseil, laissez tout le matériel installé, ou dans une boîte et si vous devez vous rappeler une chose importante comme 
						un mot de passe, une idée, etc., prenez rapidement des notes. </li>
						<li><strong>vous devez faire un travail très long qui demande beaucoup de concentration </strong>? Organisez des petites pauses de 5 à 10 minutes qui vous changeront complètement les idées. 
						C'est très bon pour les idées, pour passer des murs qui semblaient infranchissables, pour éviter l'overdose et pour la santé tout simplement. </li>
						<li> passez une tâche si vraiment c'est plus fort que vous, revenez y plus tard. Vous serez peut-être mieux disposé à ce moment ? Mais attention à la procrastination ! 😀️</li> 
						<li> <strong>archivez le moins urgent </strong>et n'y pensez plus,</li> 
						<li> etc...</li> 
						</ul>				  
					  </p>
					</div><br>
							
					<button onclick="accordion('sujet5')" class="w3-button w3-block w3-left-align w3-large w3-padding-24 w3-lime w3-card-2  w3-round"><b> 🦋 Conseils et mise au point </b></button>
					<div id="sujet5" class="w3-container w3-hide">
					  <p>
					    Quelques points sont à noter :
					    <ul>
					    <li>Si vous testez <strong>Minute Papillon</strong>, peut-être que vous serez atteint du syndrome du <strong>« tout nouveau tout beau »</strong> associé à celui du 
					    <strong> « j'ai enfin trouvé ce qui va  rendre ma vie géniale » </strong> et vous tomberez dans le piège du <strong> « j'en parle à tout le monde autour de moi, 
					    je vis, je mange, je dors avec » </strong> à tel point que vous aurez du mal à revenir en arrière de peur de reconnaître que vous vous êtes tellement trompé
					    et vous perdrez beaucoup de temps avant le <strong> « j'en ai marre de ce truc qui me prend la tête »</strong>. Donc restez critique.<br>
					    <br>Plus sérieusement, parfois vouloir à tout  prix utiliser une liste circulaire de tâches  peut provoquer les effets inverses de ce qui est attendu. Alors, n'ayez pas peur d'en avoir marre et de revenir à 
					    la ligne droite. Je serais même tentée de vous conseiller d'alterner. Mais, qui suis-je pour dire cela ? Peut-être que le cercle est la solution idéale de certains ? 
					     <br>
					    La liste circulaire de tâches est utile tant qu'elle n'est pas pesante et qu'elle ne devient pas une obsession ou une obligation. Ne l'imposez à personne pour les mêmes raisons.
					    Cet outil devrait  être  une idée ponctuelle pour passer un cap,  faciliter un travail,  diversifier des centres d'intérêt, donner une chance à des idées, terminer ce qui ne
					    l'a jamais été et tout ce que vous pouvez y trouver. Faire le café ? je ne sais pas ?<br><br> </li>
					    <li><strong>Limitez les listes de tâches courtes et des tâches longues actives</strong> afin de pouvoir tout faire et si nécessaire repasser sur les petites tâches une ou deux fois selon les besoins.
					    Sinon,  vous serez découragé.<br> Le but d'une liste c'est aussi de ne plus penser aux tâches reportées.  
					    Vous pourrez changer  votre liste un autre jour.<br><br></li>
					    <li><strong>Prévoyez des journées libres</strong>, sans tâches organisées pour vous libérer l'esprit.<br><br></li>
					    <li><strong>Faites au mieux, comme vous le sentez !</strong> <br><br></li>
					    </ul>
					  </p>
					</div><br>
						
					<!--<button onclick="accordion('sujet6')" class="w3-button w3-block w3-left-align w3-large w3-padding-24 w3-lime w3-card-2  w3-round"><b> 🦋 Tutoriel </b></button>
					<div id="sujet6" class="w3-container w3-hide">
					  <p></p>
					</div><br>-->
													
         </div>
	</div>
<?php
include("_footer.inc.php");
?>