<?php
/*Le Procrastinateur -- 06/09/2020
 *
 * gestion.php
 * Auteur : Acryline Erin
 * Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 3.0 France.
 * 
 */

 include("_header.inc.php");
 $message['nouvelle_tache'] = "";
 
 if(isset($_POST['nouvelle_tache']) && isset($_POST['emoji']) &&
 isset($_POST['type_liste']) && isset($_POST['statut']))
 {
   $message['nouvelle_tache'] = enregistrerTache($_POST); 
   $tabTaches = json_decode($_SESSION['Liste'],TRUE);
 }elseif( isset($_POST['nouvelle_tache']) &&  !isset($_POST['emoji'])) 
 {
 	$message['nouvelle_tache'] = html_entity_decode("<span class='w3-padding w3-text-red'>Veuillez sélectionner un émoji.</span>");
 }
 
 if(isset($_POST['enregistrer_chrono']) && isset($_POST['tps_court']) && isset($_POST['tps_long']))
 {
   $message['timers'] = enregistrerTimers($_POST);
   $tpsCourt = $_SESSION['tpsCourt'];
   $tpsLong = $_SESSION['tpsLong'];
 }
 
  if(isset($_POST['purger_data']))
 {
  $message['init'] = initialiserData($_POST); 
  $tabTaches = array();
  $tpsCourt = 8;
  $tpsLong = 40;
 }

 if(isset($_POST['supprimer_tache']) && isset($_POST['id_tache']) )
 {
	  if($_POST['id_tache'] != "")
	  {
      supprimerTache($_POST);
      $tabTaches = json_decode($_SESSION['Liste'],TRUE);
    }
 }

if(isset($_POST['modifier_tache']) && isset($_POST['id_tache']) )
{
	  if($_POST['id_tache'] != "")
	  {
      modifierTache($_POST);
      $tabTaches = json_decode($_SESSION['Liste'],TRUE);
    }
}

?>
<div class="w3-row">
	<div class="w3-half ">
		  <?php afficherTabTaches($tabTaches);?>
	</div>
	<div class="w3-half ">
      <?php 
      nouvelleTache($message['nouvelle_tache']);
      //sauvegarderListes();
      //restaurerListes();
      gestionChronos($tpsCourt,$tpsLong);
      echo "<div id='purger'>"; 
        purgerDonnees();
      echo "</div>";
      ?>
	</div>
</div>
<?php include("_footer.inc.php");?>