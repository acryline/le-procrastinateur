<!--
 *Le Procrastinateur -- 06/09/2020
 *
 * _footer.inc.php
 * Auteur : Acryline Erin
 * Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 3.0 France.
 * 
 */ 
 -->
   <script src="js/chrono.js"  async ></script>
 	<script src="js/script.js" defer async ></script>
	<footer class='footer'>
		<div class='w3-tiny' style='padding-left:20px;'>
			<br><a rel='license' href='http://creativecommons.org/licenses/by-nc/3.0/fr/'>
			<img alt='Licence Creative Commons' style='border-width:0' src='https://i.creativecommons.org/l/by-nc/3.0/fr/88x31.png' /></a>
			Acryline-Erin -- Septembre 2020-- Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 3.0 France</a>.
		</div>
	</footer>
	</section>
</body>	
</html>